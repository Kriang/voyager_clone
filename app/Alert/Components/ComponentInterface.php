<?php

namespace App\Alert\Components;

interface ComponentInterface
{
    public function render();
}
